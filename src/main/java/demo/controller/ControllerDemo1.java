package demo.controller;


import demo.generated.ShiporderV1;
import demo.generated.ShiporderV2;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

/**
 * Created by tomask79 on 06.03.17.
 */
@RestController
public class ControllerDemo1 {

    @RequestMapping(value="/createShipmentOrder/v1", method = RequestMethod.POST)
    public void processVersion1(@RequestBody ShiporderV1 shipOrder) throws Exception{
        System.out.println("V1 object");
        JAXBContext jc = JAXBContext.newInstance(ShiporderV1.class);

        Marshaller marshaller = jc.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(shipOrder, System.out);
    }

    @RequestMapping(value="/createShipmentOrder/v2", method = RequestMethod.POST)
    public void processVersion2(@RequestBody ShiporderV2 shipOrder) throws Exception{
        System.out.println("V2 object");
        JAXBContext jc = JAXBContext.newInstance(ShiporderV2.class);

        Marshaller marshaller = jc.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(shipOrder, System.out);
    }
 }
