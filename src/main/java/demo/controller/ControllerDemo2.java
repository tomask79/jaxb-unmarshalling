package demo.controller;

import demo.generated2.Shiporder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

/**
 * Created by tomask79 on 08.03.17.
 */
@RestController
public class ControllerDemo2 {
    @RequestMapping(value="/createShipmentOrder2/v1", method = RequestMethod.POST)
    public void processVersion1(@Valid @RequestBody Shiporder shipOrder) throws Exception{
        JAXBContext jc = JAXBContext.newInstance(Shiporder.class);

        Marshaller marshaller = jc.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(shipOrder, System.out);
    }

}
