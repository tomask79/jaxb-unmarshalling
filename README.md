# JAXB XML unmarshalling with backward and forward compatibility #

If you want to do [continuous delivery](https://martinfowler.com/bliki/ContinuousDelivery.html) with your system then you're forced to be solving problem of backward and forward compatibility of your communication with surrounding systems. Why? For example: 

* You cannot upgrade to the new version of the communication XML.
* Or your part of the task is not done yet.

Let's show an easy way of how to do it, how to consume newer version of communication XML and still be able to work. Assumptions for achieving backward and forward compatibility:

* **You only add new fields to your communication JAXB stubs**
* **You DO NOT delete or rename old fields.**

This process is called ["Duck typing"](https://en.wikipedia.org/wiki/Duck_typing)

## UnMarshalling with forward and backward compatibility ##

You use simple **JAXB unmarshalling with NO schema or validation set.** JAXB unmarshaller has following capabilities very usable for backward and forward compatibility ([from Doc](http://docs.oracle.com/javaee/7/api/javax/xml/bind/JAXB.html)): 

*Schema validation is not performed on the input XML. The processing will try to continue even if there are errors in the XML, as much as possible. Only as the last resort, this method fails with DataBindingException.*

With this way of unmarshalling you'll handle typical situation which will occur during development: Someone added new field to the stub object 

## Validating the unmarshalled result from previous step ##

You've got several options:

* Manual validation of the JAXB stub (Easy to implement, but maybe harder to maintain. Depends on the app)
* Generate JAXB stub with JSR 303 annotations on that. See [this conversation](http://stackoverflow.com/questions/5838133/validation-for-generated-jaxb-classes-jsr-303-spring) for example.

## Unmarshalling new version of XML with older stub (demo) ##

Lets have following XML (version 1)

```
<?xml version="1.0" encoding="utf-8"?>
<shiporder orderid="str1234">
    <orderperson>str1234</orderperson>
    <shipto>
        <name>str1234</name>
        <address>str1234</address>
        <city>str1234</city>
        <country>str1234</country>
    </shipto>
    <item>
        <title>str1234</title>
        <note>str1234</note>
        <quantity>745</quantity>
        <price>123.45</price>
    </item>
</shiporder>
```

with **item** object:
```
.
.
   @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "title",
            "note",
            "quantity",
            "price"
    })
    public static class Item {

        @XmlElement(required = true)
        protected String title;
        protected String note;
        @XmlElement(required = true)
        @XmlSchemaType(name = "positiveInteger")
        protected BigInteger quantity;
        @XmlElement(required = true)
        protected BigDecimal price;
.
.
```
and version 2 with NEW additional required parameter.
```
<?xml version="1.0" encoding="utf-8"?>
<shiporder orderid="str1234">
    <orderperson>str1234</orderperson>
    <shipto>
        <name>str1234</name>
        <address>str1234</address>
        <city>str1234</city>
        <country>str1234</country>
    </shipto>
    <item>
        <title>str1234</title>
        <note>str1234</note>
        <originCountry>Spain</originCountry>
        <quantity>745</quantity>
        <price>123.45</price>
    </item>
</shiporder>
```
with JAXB presentation:


```
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "title",
        "note",
        "originCountry",
        "quantity",
        "price"
    })
    public static class Item {

        @XmlElement(required = true)
        protected String title;
        protected String note;
        @XmlElement(required = true)
        protected String originCountry;
        @XmlElement(required = true)
        @XmlSchemaType(name = "positiveInteger")
        protected BigInteger quantity;
        @XmlElement(required = true)
        protected BigDecimal price;
.
.
```
and following Rest Controller:


```
package demo.controller;


import demo.generated.ShiporderV1;
import demo.generated.ShiporderV2;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

/**
 * Created by tomask79 on 06.03.17.
 */
@RestController
public class ControllerDemo {

    @RequestMapping(value="/createShipmentOrder/v1", method = RequestMethod.POST)
    public void processVersion1(@RequestBody ShiporderV1 shipOrder) throws Exception{
        System.out.println("V1 object");
        JAXBContext jc = JAXBContext.newInstance(ShiporderV1.class);

        Marshaller marshaller = jc.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(shipOrder, System.out);
    }

    @RequestMapping(value="/createShipmentOrder/v2", method = RequestMethod.POST)
    public void processVersion2(@RequestBody ShiporderV2 shipOrder) throws Exception{
        System.out.println("V2 object");
        JAXBContext jc = JAXBContext.newInstance(ShiporderV2.class);

        Marshaller marshaller = jc.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(shipOrder, System.out);
    }
 }

```
Now lets **send Version 2 of XML with new "originCountry" attribute to Version 1 endpoint**. Output is going to be:
```
V1 object
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<shiporder orderid="str1234">
    <orderperson>str1234</orderperson>
    <shipto>
        <name>str1234</name>
        <address>str1234</address>
        <city>str1234</city>
        <country>str1234</country>
    </shipto>
    <item>
        <title>str1234</title>
        <note>str1234</note>
        <quantity>745</quantity>
        <price>123.45</price>
    </item>
</shiporder>
```
As you can see new attribute from version 2 was ignored by version 1 endpoint and XML was unmarshalled.

## Validating JAXB stubs with JSR 303 ##

Popular for this purpose is [krasa-jaxb-tool plugin](https://github.com/krasa/krasa-jaxb-tools). Lets plug it into our project:


```
			<plugin>
				<groupId>org.jvnet.jaxb2.maven2</groupId>
				<artifactId>maven-jaxb2-plugin</artifactId>
				<executions>
					<execution>
						<goals>
							<goal>generate</goal>
						</goals>
						<configuration>
							<forceRegenerate>true</forceRegenerate>
							<generatePackage>demo.generated2</generatePackage>
							<generateDirectory>${basedir}/src/main/java/</generateDirectory>
							<schemas>
								<schema>
									<fileset>
										<directory>${basedir}/src/main/resources/xsd</directory>
										<includes>
											<include>*.*</include>
										</includes>
										<excludes>
											<exclude>*.xs</exclude>
										</excludes>
									</fileset>
								</schema>
							</schemas>
							<extension>true</extension>
							<args>
								<arg>-XJsr303Annotations</arg>
							</args>
							<plugins>
								<plugin>
									<groupId>com.github.krasa</groupId>
									<artifactId>krasa-jaxb-tools</artifactId>
									<version>1.5</version>
								</plugin>
							</plugins>
						</configuration>
					</execution>
				</executions>
			</plugin>

```
This plugin not only generates JAXB stub but it adds JSR 303 to it so version 1 of Item looks now:


```
.
.
   @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "title",
        "note",
        "quantity",
        "price"
    })
    public static class Item {

        @XmlElement(required = true)
        @NotNull
        protected String title;
        protected String note;
        @XmlElement(required = true)
        @XmlSchemaType(name = "positiveInteger")
        @NotNull
        @DecimalMin("1")
        protected BigInteger quantity;
        @XmlElement(required = true)
        @NotNull
        protected BigDecimal price;
.
.
```
Lets create another controller similar to previous V1 controller, but this time lets trigger JSR 303 validation on the JAXB stub:

```
@RestController
public class ControllerDemo2 {
    @RequestMapping(value="/createShipmentOrder2/v1", method = RequestMethod.POST)
    public void processVersion1(@Valid @RequestBody Shiporder shipOrder) throws Exception{
        JAXBContext jc = JAXBContext.newInstance(Shiporder.class);

        Marshaller marshaller = jc.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(shipOrder, System.out);
    }

}
```
Now lets post version of XML with additional attribute originCountry to the new version 1 endpoint, output is:

```
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<shiporder orderid="str1234">
    <orderperson>str1234</orderperson>
    <shipto>
        <name>str1234</name>
        <address>str1234</address>
        <city>str1234</city>
        <country>str1234</country>
    </shipto>
    <item>
        <title>str1234</title>
        <note>str1234</note>
        <quantity>745</quantity>
        <price>123.45</price>
    </item>
</shiporder>
```
XML is complete, no error, additional attribute from newer version was ignored.
Lets post invalid XML with some required attribute missing, like:

```
<?xml version="1.0" encoding="utf-8"?>
<shiporder orderid="str1234">
    <orderperson>str1234</orderperson>
    <shipto>
        <name>str1234</name>
        <address>str1234</address>
        <city>str1234</city>
        <country>str1234</country>
    </shipto>
    <item>
        <!--<title>str1234</title>-->
        <originCountry>SPAIN</originCountry>
        <note>str1234</note>
        <quantity>745</quantity>
        <price>123.45</price>
    </item>
</shiporder>
```

Output is now:

```
2017-03-08 23:47:58.684  WARN 794 --- [nio-8080-exec-3] .w.s.m.s.DefaultHandlerExceptionResolver : Handler execution resulted in exception: Validation failed for argument at index 0 in method: public void demo.controller.ControllerDemo2.processVersion1(demo.generated2.Shiporder) throws java.lang.Exception, with 1 error(s): [Field error in object 'shiporder' on field 'item[0].title': rejected value [null]; codes [NotNull.shiporder.item[0].title,NotNull.shiporder.item.title,NotNull.item[0].title,NotNull.item.title,NotNull.title,NotNull.java.lang.String,NotNull]; arguments [org.springframework.context.support.DefaultMessageSourceResolvable: codes [shiporder.item[0].title,item[0].title]; arguments []; default message [item[0].title]]; default message [nesmí být null]] 
```

JSR 303 with JAXB is certainly fine way to go if you want to keep version compatibility. Check it for yourself.

regards

Tomas